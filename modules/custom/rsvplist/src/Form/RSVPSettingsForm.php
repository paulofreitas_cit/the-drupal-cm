<?php

/**
 * @file
 * Contains \Drupal\rsvplist\Form\RSVPSettingsForm
 */

namespace Drupal\rsvplist\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

class RSVPSettingsForm extends ConfigFormBase {
  public function getFormId() {
    return 'rsvp_admin_settings';
  }

  public function getEditableConfigNames() {
    return ['rsvplist.settings'];
  }

  public function buildForm(array $form, FormStateInterface $form_state, Request $request = null)
  {
    $types = node_type_get_names();
    $config = $this->config('rsvplist.settings');
    $form['rsvplist_types'] = [
      '#title' => $this->t('The content types to enable RSVP collection'),
      '#type' => 'checkboxes',
      '#default_value' => $config->get('allowed_types'),
      '#options' => $types,
      '#description' => 'On the specified node types, an RSVP option will be available and can be enabled while that node is being edited.',
    ];

    $form['array_filter'] = ['#type' => 'value', '#value' => true];
    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $allowed_types = array_filter($form_state->getValue('rsvplist_types'));
    sort($allowed_types);
    $this->config('rsvplist.settings')
      ->set('allowed_types', $allowed_types)
      ->save();
    parent::submitForm($form, $form_state);
  }
}
