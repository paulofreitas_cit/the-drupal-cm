<?php

/**
 * @file
 * Constains \Drupal\rsvplist\Form\RSVPForm
 */

namespace Drupal\rsvplist\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides as RSVP Email form
 */
class RSVPForm extends FormBase
{
  public function getFormId()
  {
    return 'rsvplist_email_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $node = \Drupal::routeMatch()->getParameter('node');
    if (isset($node)) {
      $nid = $node->id();
    }

    $form['email'] = [
      '#title' => 'Email Adress',
      '#type' => 'textfield',
      '#size' => 25,
      '#description' => 'We1ll send updates to he email adress you provide.',
      '#required' => TRUE
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'RSVP'
    ];

    $form['nid'] = [
      '#type' => 'hidden',
      '#value' => $nid
    ];

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValue('email');
    if ($value == !\Drupal::service('email.validator')->isValid($value)) {
      $form_state->setErrorByName(
        'email',
        'The email address ' . $value . ' is not valid'
      );
      return;
    }
    $node = \Drupal::routeMatch()->getParameter('node');

    $select = Database::getConnection()->select('rsvplist', 'r');
    $select->fields('r', ['nid']);
    $select->condition('nid', $node->id());
    $select->condition('mail', $value);

    $results = $select->execute();
    if (!empty($results->fetchCol())) {
      $form_state->setErrorByName('email', 'The address ' . $value . ' is already subscribed to this list. :/');
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    \Drupal::database()->insert('rsvplist')->fields([
      'mail' => $form_state->getValue('email'),
      'nid' => $form_state->getValue('nid'),
      'uid' => $user->id(),
      'created' => time()
    ])->execute();

    \Drupal::messenger()->addMessage('Thank you for you RSVP, you are on the list for the event. :)))');
  }
}
