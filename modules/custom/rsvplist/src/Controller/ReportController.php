<?php

/**
 * @file
 * Contains \Drupal\rsvplist\Controller\ReportController
 */

namespace Drupal\rsvplist\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Component\Utility\Html;

class ReportController extends ControllerBase {

  protected function load() {
    $select = Database::getConnection()->select('rsvplist', 'r');
    $select->join('users_field_data', 'u', 'r.uid = u.uid');
    $select->join('node_field_data', 'n', 'r.nid = n.uid');

    $select->addField('u', 'name', 'username');
    $select->addField('n', 'title');
    $select->addField('r', 'mail');

    $entries = $select->execute()->fetchAll(\PDO::FETCH_ASSOC);

    return $entries;
  }

  public function report()
  {
    $content = [];
    $content['message'] = [
      '#markup' => $this->t('Below is a list of all Event RSVPs including username, email address and the name of the event they will be attending.')
    ];

    $headers = ['Name', 'Event', 'Email'];
    $rows = [];

    foreach ($entries = $this->load() as $entry) {
      $obj_html = new Html;

      $rows[] = array_map(function ($entry) use ($obj_html) {
        return $obj_html::escape($entry);
      }, $entry);
    }

    $content['table'] = array(
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => 'No entries available.'
    );

    $content['#cache']['max-age'] = 0;

    return $content;
  }
}
